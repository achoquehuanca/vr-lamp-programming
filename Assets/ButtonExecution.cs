﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine.UI;
using UnityEngine;

public class ButtonExecution : MonoBehaviour
{
    public Transform opcion1;
    public Transform opcion2;
    public Transform opcion3;
    public Transform opcion4;
    RaycastHit hit;
    public LayerMask RayMask;
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        PressButton();
    }

    void PressButton()
    {
        if (Input.GetKeyDown(KeyCode.Mouse0))
        {
            Debug.Log("Click Presionado");
            if (Physics.Raycast(transform.position, transform.forward, out hit, 10.0f, RayMask))
            {
                Debug.Log("Boton Apuntado");
                if (hit.transform.tag == "BtnExecute")
                {
                    Debug.Log("Opcion1=" + opcion1.position);
                    Debug.Log("Opcion2=" + opcion2.position);
                    Debug.Log("Opcion3=" + opcion3.position);
                    Debug.Log("Opcion4=" + opcion4.position);
                }
            }
        }

        /*if (Input.GetKeyDown(KeyCode.Mouse1))
        {
            RemoveTransform();
        }

        if (CurrentTransform)
            MoveTransformAround();*/
    }
}
