﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerLook : MonoBehaviour
{
    public Transform opcion1;
    public Transform opcion2;
    public Transform opcion3;
    public Transform opcion4;

    public bool focoBueno;
    public bool lamparaEnchufada;

    public GameObject lampON;
    public GameObject lampOFF;
    public GameObject buttonON;
    public GameObject buttonOFF;

    public Transform PlayerBody;
    public float MouseSensitivity;
    public LayerMask RayMask;

    float xAxisClamp = 0.0f;
    RaycastHit hit;
    Transform CurrentTransform;
    float length;
    // Start is called before the first frame update
    void Awake()
    {
        Cursor.lockState = CursorLockMode.Locked;
    }

    // Update is called once per frame
    void Update()
    {
        RotCam();
        PickUpObj();
        PressButton();
    }

    void RotCam() 
    {
        float MouseX = Input.GetAxis("Mouse X");
        float MouseY = Input.GetAxis("Mouse Y");

        float RotationX = MouseX * MouseSensitivity;
        float RotationY = MouseY * MouseSensitivity;

        Vector3 TargetRotCam = transform.rotation.eulerAngles;
        Vector3 TargetRotBody = PlayerBody.rotation.eulerAngles;

        xAxisClamp -= RotationY;

        TargetRotCam.x -= RotationY;
        TargetRotCam.z = 0;
        TargetRotBody.y += RotationX;

        if (xAxisClamp > 90f)
        {
            xAxisClamp = 90f;
            TargetRotCam.x = 90f;
        }
        else if (xAxisClamp < -90f)
        {
            xAxisClamp = -90f;
            TargetRotCam.x = -90;
        }

        transform.rotation = Quaternion.Euler(TargetRotCam);
        PlayerBody.rotation = Quaternion.Euler(TargetRotBody);
        //Debug.Log(TargetRotCam);
        //Debug.DrawRay(this.transform.position, Vector3.forward * 10f, Color.green);
    }

    void PickUpObj()
    {
        if(Input.GetKeyDown(KeyCode.Mouse0))
        {
            if(Physics.Raycast(transform.position, transform.forward, out hit, 10.0f, RayMask))
            {
                if(hit.transform.tag == "MoveOBJ")
                {
                    SetNewTransform(hit.transform);
                }
            }
        }

        if (Input.GetKeyDown(KeyCode.Mouse1))
        {
            RemoveTransform();
        }

        if (CurrentTransform)
            MoveTransformAround();
    }

    public void SetNewTransform(Transform newTransform)
    {
        if (CurrentTransform)
            return;
        CurrentTransform = newTransform;
        length = Vector3.Distance(transform.position, newTransform.position);
        CurrentTransform.GetComponent<Rigidbody>().isKinematic = true;
    }

    private void MoveTransformAround()
    {
        CurrentTransform.position = transform.position + transform.forward * length;
    }

    public void RemoveTransform()
    {
        if (!CurrentTransform)
            return;
        //CurrentTransform.GetComponent<Rigidbody>().isKinematic = false;
        CurrentTransform = null;
    }

    void PressButton()
    {
        if (Input.GetKeyDown(KeyCode.Mouse0))
        {
            //Debug.Log("Click Presionado");
            if (Physics.Raycast(transform.position, transform.forward, out hit, 10.0f, RayMask))
            {
                //Debug.Log("Boton Apuntado");
                if (hit.transform.tag == "BtnExecute")
                {
                    if(((opcion1.position.x>-0.65 && opcion1.position.x < -0.55)
                        && (opcion1.position.y > 1.55 && opcion1.position.x < 1.65)) ||
                        ((opcion1.position.x > -0.65 && opcion1.position.x < -0.55)
                        && (opcion1.position.y > 1.45 && opcion1.position.x < 1.55)))
                            focoBueno = false;

                    if (((opcion2.position.x > -0.65 && opcion2.position.x < -0.55)
                        && (opcion2.position.y > 1.55 && opcion2.position.x < 1.65)) ||
                        ((opcion2.position.x > -0.65 && opcion2.position.x < -0.55)
                        && (opcion2.position.y > 1.45 && opcion2.position.x < 1.55)))
                            lamparaEnchufada = false;

                    if (((opcion3.position.x > -0.65 && opcion3.position.x < -0.55)
                        && (opcion3.position.y > 1.55 && opcion3.position.x < 1.65)) ||
                        ((opcion3.position.x > -0.65 && opcion3.position.x < -0.55)
                        && (opcion3.position.y > 1.45 && opcion3.position.x < 1.55)))
                            focoBueno = true;

                    if (((opcion4.position.x > -0.65 && opcion4.position.x < -0.55)
                        && (opcion4.position.y > 1.55 && opcion4.position.x < 1.65)) ||
                        ((opcion4.position.x > -0.65 && opcion4.position.x < -0.55)
                        && (opcion4.position.y > 1.45 && opcion4.position.x < 1.55)))
                            lamparaEnchufada = true;

                    Debug.Log("focoBueno=" + focoBueno);
                    Debug.Log("lamparaEnchufada="+lamparaEnchufada);

                    Debug.Log("Opcion1=" + opcion1.position);
                    Debug.Log("Opcion2=" + opcion2.position);
                    Debug.Log("Opcion3=" + opcion3.position);
                    Debug.Log("Opcion4=" + opcion4.position);

                    if (focoBueno == true)
                    {
                        if (lamparaEnchufada == true)
                        {
                            buttonON.SetActive(true);
                            lampON.SetActive(true);
                            buttonOFF.SetActive(false);
                            lampOFF.SetActive(false);
                        }
                        else
                        {
                            buttonON.SetActive(false);
                            lampON.SetActive(false);
                            buttonOFF.SetActive(true);
                            lampOFF.SetActive(true);
                        }
                    }
                    else
                    {
                        if (lamparaEnchufada == true)
                        {
                            buttonON.SetActive(true);
                            lampON.SetActive(false);
                            buttonOFF.SetActive(false);
                            lampOFF.SetActive(true);
                        }
                        else
                        {
                            buttonON.SetActive(false);
                            lampON.SetActive(false);
                            buttonOFF.SetActive(true);
                            lampOFF.SetActive(true);
                        }
                    }
                }
            }
        }

        /*if (Input.GetKeyDown(KeyCode.Mouse1))
        {
            RemoveTransform();
        }

        if (CurrentTransform)
            MoveTransformAround();*/
    }
}
