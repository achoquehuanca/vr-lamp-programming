﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerMove : MonoBehaviour
{
    CharacterController CharControl;
    public float WalkSpeed = 10f;

    void Awake() 
    {
        CharControl = GetComponent<CharacterController>();
    }
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        MovePlayer();
    }

    void MovePlayer()
    {
        float horiz = Input.GetAxis("Horizontal");
        float vert = Input.GetAxis("Vertical");

        Vector3 MoveDirSide = transform.right * horiz * WalkSpeed;
        Vector3 MoveDirForward = transform.forward * vert * WalkSpeed;

        CharControl.SimpleMove(MoveDirSide);
        CharControl.SimpleMove(MoveDirForward);
    }
}
