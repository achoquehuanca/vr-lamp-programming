﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class JumpGesture : MonoBehaviour
{
    private HeadGesture gesture;

    private CharacterController controller;


    public bool isJump = false;
    public float jumpForce = 100.0f;
    private float jumpRate = 1.0f;
    private float previousHeight;
    private HeadLookWalkBounce walkBounce;



    
    //private GameObject dashboard;
    private bool isOpen = true;
    private Vector3 startRotation;
    //Transform myCamera = Camera.main.transform;
    // Start is called before the first frame update
    void Start()
    {
        controller = GetComponent<CharacterController>();

        gesture = GetComponent<HeadGesture>();

        


        previousHeight = Camera.main.transform.position.y;
        walkBounce = GetComponent<HeadLookWalkBounce>();

        
        //dashboard = GameObject.Find("Dashboard");
        //startRotation = dashboard.transform.eulerAngles;

    }

    // Update is called once per frame
    void Update()
    {
        //if (DetectJump())
        //if (Camera.main.transform.rotation.x <= 0.0)
        if (false)
        {
            walkBounce.bounceForce = jumpForce;
        }
    }
    private bool DetectJump()
    {
        float height = Camera.main.transform.localPosition.y;
        float deltaHeight = height - previousHeight;
        float rate = deltaHeight / Time.deltaTime;
        previousHeight = height;
        return (rate >= jumpRate);
    }
}
